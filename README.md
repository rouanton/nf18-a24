# NF18-A24

Membres du Groupe 4 TD 2 :
- Arthur Maugee
- Hamed Zie Kone
- Hafid Mohamed-Yassine
- Antony Roux



+ Rendu 1 : Nous avons tous contribué à la conception du modèle conceptuel. Antony s’est chargé de la réalisation du schéma UML et de la rédaction de la note de clarification.

+ Rendu 2 : Nous nous sommes répartis équitablement la rédaction du modèle logique en nous répartissant les tables. Antony a géré la transformation des héritages liés au personnel, Arthur a géré les relations liées au dossier médical, Yassine a géré les relations liées aux patients, et Hamed a géré les relations liées aux classes d’associations de cardinalité M:N.

+ Rendu 3 : Hamed et Arthur ont clarifié les ambiguïtés dans le modèle logique, tandis que Yassine et Antony ont rédigé les requêtes SQL de création et d’insertion.

+ Rendu 4 : Antony a commencé à coder l'application en Python, tandis que les autres ont corrigé les requêtes SQL.

+ Rendu 5 : Arthur s’est occupé de l’interface de l’application destinée aux clients, Hamed s’est occupé de l’interface destinée à l’assistant, Yassine s’est chargé de l’interface destinée à l’administrateur, et Antony a pris en charge l’interface du vétérinaire.

+ Rendu 6 : Hamed et Arthur se sont occupés de la conception du modèle orienté objet et de la création de la collection "patients". Antony s’est chargé du fichier d’insertion, et Yassine a travaillé sur le fichier contenant les autres requêtes liées au nouveau modèle.

Pour chaque rendu, chaque membre du groupe a effectué une charge de travail équivalente, c’est-à-dire que chacun a contribué à hauteur de 25 % pour chaque rendu tout au long du semestre.