﻿Tous les attributs sont NOT NULL par défauts sauf indiqué


Personnels (#id_personnel : integer ,nom : string, prenom : string, date_naissance : date, adresse : string, telephone : string) 


Clients (#id_client : integer, nom : string, prenom : string, date_naissance : date, adresse : string, telephone : string) 


Suivi (#veterinaire => Veterinaires, #patient => Patients, start : date, end : date) avec end pouvant être NULL


Appartient (#proprietaire => Clients, #animal => Patients, start : date, end : date) avec end pouvant être NULL


Patients (#id_patient : integer, nom : string, date_naissance : date, id : integer, passport : string, idEspece=>Especes, idDossierMed=>Dossier_med) avec id, date_naissance et passeport pouvant être NULL


Medicaments (#nom : string, effet : text)


Assistants(#id_assistant=>Personnels)


Veterinaires (#id_veterinaire=>Personnels)


Quantite (#traitement => Traitement, #medicament => Medicaments, nbr : integer)


Traitements (#id_traitement : integer, start : date, duree : integer, date : datetime, idVeterinaire=>Veterinaire, idDosser_Med=>Dossier_Med)


Especes (#type : string, categorie:{“félin”, “canidé”, “reptile”, “rongeurs”, “oiseaux”, “autres”} )


Dossier_med (#id_med : integer, idProc=>Procedures, idMesure => Mesures, idAnalyse=>Anlalyses, idObservation => Observations)   (IdProc, IdMesure, IdAnalyse, IdOberservation) pouvant être NULL.


Analyses (#id_analyses : integer, lien : string, date : datetime, dossier_med=>Dossier_med)


Observations (#idObservations : integer, observation : string, date : datetime, dossier_med=>Dossier_med)


Procedures (#id_procedures : integer, desc : text, date : datetime, dossier_med=>Dossier_med)


Mesures (id_mesures : integer, dossier_med=>Dossier_med, taille : integer, date_taille : datetime, poids : integer, date_poids : datetime) avec (taille NULL si date_taille NULL) et (poids NULL si date_poids NULL)




Autorise (#espece => Especes, #medicament => Medicaments)


Specialise ( #personnel => Personnels, #espece => Especes)










Justification choix héritage : 
Heritage par classe mère (Tailles et Poids —> Mesures) car héritage complet et héritage non-exclusif (une mesure peut être le poids et la taille)


Heritage par classe fille (Humains <-– Personnels et Clients) car classe mère abstraite et héritage non complet


Heritage par references (Personnels <— Veterinaires et Assistants) car il y a une association sur la classe mère et l’héritage est non-complet